from django.db import models
from django.contrib import admin
# Create your models here.

class Certificate(models.Model):
    name=models.CharField(max_length=245)
    college=models.CharField(max_length=245)
    dept_name=models.CharField(max_length=245)
    start=models.CharField(max_length=245)
    end=models.CharField(max_length=245)

    def __str__(self):
        return self.name